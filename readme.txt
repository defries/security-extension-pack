=== Must Use Security ===
Contributors: forsitemedia
Tags: security, password, validation, protection
Requires at least: 3.4
Tested up to: 3.7
Stable tag: 0.2
License: GPLv2

Add an extra security layer protecting all WordPress users on several crucial levels.

== Description ==

Must Use Security is a plugin for every WordPress website. It enhances the security level for users and complies
with international standards. This is the best solution for setting low-level protection to your users, their
login policy and password rules.

This plugin uses code from the following plugins:
* auto-expire-passwords by contributers DJPaul, telegraph
* force-password-change by contributer lumpysimon
* force-strong-passwords by contributers gyrus, simonwheatley, sparanoid
* user-locker by contributer sirzooro

Code from plugins is modified, extended, enhanced or simplified where needed.
Docblocks have been added or improved where needed.

We thank the contributers of these plugins for their excellent work.

Features include:

* Previous login information - display the last time the user logged into the system, together with last IP and hostname
* User account disabling - manually lock or disable an account from the administrator of the website
* Lock after consecutive failures - lock the user account after 5 consecutive incorrect password login attempts
* Enforce strong passwords - validate password strength and allow only strong passwords
* Minimum length passwords - disallow password updates for passwords shorter than 8 characters
* Enforce password change - when the admin creates a new user, the user should update its password before getting authorized to the website
* Enforce password valid time - the system ensures that passwords do not remain valid for more than 90 (ninety) days
* Validate against old passwords - disallow users to set one of their last 12 passwords (extra hashing used for storage)

== Installation ==

1. Install the Must Use Security plugin
2. All rules are automatically applied to the WordPress website life cycle

== Changelog ==

= 0.2 =
* Added credits to contributers of code used from other plugins
* Removed original plugins
* Show display name in dashboard widget
* Do not set error message if user only changes password

= 0.1 =
* Add all core features to the Must Use Security plugin