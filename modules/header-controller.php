<?php

/**
 *
 * Helper class for managing headers for security reasons.
 *
 *
 * @since 0.2
 */
class ForSite_Header_Controller {
	public function __construct() {
		add_filter( 'wp_headers', array( $this, 'add_xframe_options' ) );
	}
	
	public function add_xframe_options( $headers ) {
		$headers['X-Frame-Options'] = 'SAMEORIGIN';
		
		return $headers;
	}
}

new ForSite_Header_Controller();