<?php
/**
 * Helper class for displaying a login message
 * above the login form.
 *
 * @since 0.1
 *
 */
class ForSite_Login_Message {

	/**
	 * Add the login filter message
	 */
	public function __construct() {
		add_filter( 'login_message', array( $this, 'rd_login_message' ) );
		add_action( 'login_head', array( $this, 'disable_autocomplete' ) );
		add_action( 'login_init', array( $this, 'start_login_buffering' ) );
		add_action( 'login_footer', array( $this, 'end_login_buffering' ) );
	}
	
	/**
	 * Start Login buffering
	 */
	function start_login_buffering( ) {
		ob_start();
	}
	
	/**
	 * End Login buffering
	 */
	function end_login_buffering() {
		$content = ob_get_clean();
	
		$content = str_replace( '<form' , '<form autocomplete="off"', $content );
	
		echo $content;
	}
	
	public function disable_autocomplete() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'fs-login-verifier', plugins_url( '/js/disable-autocomplete.js', __FILE__ ), array( 'jquery' ) );
	}

	/**
	 * Displays a custom login message
	 *
	 * @param  string $message Login message
	 * @return string 		   Custom login message
	 */
	public function rd_login_message( $message ) {
		if ( empty($message) ){
			return '<p class="message">Unauthorized access or usage of this system in breach of internal policies is prohibited and may result in legal or disciplinary action being taken against you.</p>';
		} else {
			return $message;
		}
	}
}

new ForSite_Login_Message();
