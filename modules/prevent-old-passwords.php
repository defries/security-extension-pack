<?php
/**
 * Prevent using one of the previous 12 passwords that the user has set.
 *
 */
class ForSite_Prevent_Passwords {
	
	/**
	 * Some security salt so that broken DB hashes would not matter
	 * 
	 * @since 0.1
	 * @access private
	 * @var string
	 */
	private $fspp_salt = 'EkS/QSL%@G&&;|]|l:/$fWd.`jqzPN60';
	
	/**
	 * Count of old passwords to keep for validating new passwords
	 * 
	 * @since 0.1
	 * @access private
	 * @var unknown_type
	 */
	private $old_passwords_count = 12;
	
	/**
	 * Initialize the password manager
	 */
	public function __construct() {
		add_action( 'user_profile_update_errors', array( $this, 'validate_against_old_passwords' ), 80, 3 );
	}
	
	/**
	 * Validate the new password set by the user, bail if this has been used before
	 * 
	 * @since 0.1
	 * @access public
	 * 
	 * @uses wp_hash()
	 * @param array $errors array of WP_Errors
	 * @param bool $update is this a password update
	 * @param WP_User $user user object
	 */
	public function validate_against_old_passwords( &$errors, $update, &$user ) {
		// Bail if non-existing user_pass or no user
		if ( empty( $user ) || ! property_exists( $user, 'user_pass' ) ) {
			//$errors->add( 'missing_password', 'Incorrect password reset attempt' );
			return;
		}
		
		// Don't enforce while creating users
		if ( ! property_exists( $user, 'ID' ) )
			return;
		
		// Get new passwords to use later
		$new_password = $user->user_pass;
		$new_extra_hashed_password = wp_hash( $new_password . $this->fspp_salt );
		
		// Get old passwords
		$old_passwords = get_user_option( 'fs_old_passwords', $user->ID );
		
		if ( empty( $old_passwords ) )
			$old_passwords = array();
		
		// Verify passwords against the old list
		foreach ( $old_passwords as $old_password ) {
			if ( $old_password === $new_extra_hashed_password ) {
				$errors->add( 'old_password', 
						"You cannot use one of your last {$this->old_passwords_count} passwords." );
				return;
			}
		}
		
		// Add the new password to the array
		$old_passwords[] = $new_extra_hashed_password;
		
		// Keep the list to the $old_passwords_count size
		while ( count( $old_passwords ) > $this->old_passwords_count ) {
			array_shift( $old_passwords );
		}
		
		update_user_option( $user->ID, 'fs_old_passwords' , $old_passwords );
	}
}

new ForSite_Prevent_Passwords();
