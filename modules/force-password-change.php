<?php
/**
 License
 -------

 Copyright (c) Lumpy Lemon Ltd. All rights reserved.

 Released under the GPL license:
 http://www.opensource.org/licenses/gpl-license.php

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 */

/**
 * Change password on first login to site after a new user is created
 * 
 */
class ForSite_Password_Change {

	/**
	 * Initialize the password locker for new users
	 */
	public function __construct() {
		add_action( 'admin_init', 			   array( $this, 'validate_password_reset' ) );
		add_action( 'admin_menu', 			   array( $this, 'reset_password_page' ) );
		add_action( 'user_register',           array( $this, 'registered' ) );
		add_action( 'personal_options_update', array( $this, 'updated' ) );
		add_action( 'current_screen',          array( $this, 'redirect' ) );
		add_action( 'admin_notices',           array( $this, 'notice' ) );
	}
	
	/**
	 * Create a reset_password_page for new users only
	 * 
	 * @since 0.1
	 * @access private
	 */
	public function reset_password_page( ) {
		if( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			if ( get_user_meta( $current_user->ID, 'force-password-change', true ) ) {
				add_submenu_page( 'options-general.php', 'User Reset Password', 
						'Password Reset', 'read', 'fs-reset-password', 
						array( $this, 'reset_password_page_cb' ) );
			}
		}
	}
	
	/**
	 * Reset password page callback, display the form for password change
	 * None of the other admin pages is available until a new password is set.
	 * 
	 * @since 0.1
	 * @access private
	 */
	public function reset_password_page_cb() {
	?>
	<style type="text/css">
		.error_messages {
			background-color: #F7EECB;
		}
		.error_messages p {
			font-size: 16px;
			padding: 15px;
		}
		.error_messages .error {
			color: red;
			font-weight: bold;
		}
	</style>
	<div class="wrap">
		<h2>Password Reset</h2>
		<div class="error_messages">
		<?php 
			// Display the error messages if set during submission and validation
			if ( defined( 'FS_PASS_RESET_MESSAGES' ) ) {
				echo FS_PASS_RESET_MESSAGES;
			}	
		?>
		</div>
		<p>Enter your new password:</p>

		<form action="" method="POST">
			<label>Password:</label>
			<input type="password" name="fs_res_password1" />
			
			<label>Password (again):</label>
			<input type="password" name="fs_res_password2" />

			<input type="hidden" name="fs_reset_password_form" value="reset" />
			<?php wp_nonce_field( 'set-new-password', 'fs_new_password' ) ?>
			<input type="submit" value="Set New Password" />
		</form>
	</div>
	<?php 
	}

	/**
	 * Add a user meta field when a new user is registered
	 * 
	 * @since 0.1
	 * @access private
	 * 
	 * @param int $user_id user ID
	 */
	function registered( $user_id ) {
		add_user_meta( $user_id, 'force-password-change', 1 );
	}

	/**
	 * Delete the user meta field when a user successfully changes their password
	 * @param int $user_id user ID
	 */
	function updated( $user_id ) {
		$pass1 = $pass2 = '';

		if ( isset( $_POST['pass1'] ) )
			$pass1 = $_POST['pass1'];

		if ( isset( $_POST['pass2'] ) )
			$pass2 = $_POST['pass2'];

		if ( $pass1 != $pass2
				|| empty( $pass1 )
				|| empty( $pass2 )
				|| false !== strpos( stripslashes( $pass1 ), "\\" ) )
			return;

		delete_user_meta( $user_id, 'force-password-change' );
	}

	/**
	 * if:
	 * - we're logged in,
	 * - we're not an admin user,
	 * - the user meta field is present,
	 * - we're on the front-end or any admin screen apart from the edit profile page,
	 * then redirect to the edit profile page
	 * 
	 * @since 0.1
	 * @access private
	 */
	function redirect() {

		global $current_user;

		if ( is_admin() ) {
			$screen = get_current_screen();
			if ( 'settings_page_fs-reset-password' == $screen->base )
				return;
		}

		if ( ! is_user_logged_in() )
			return;

		if ( current_user_can( 'activate_plugins' ) )
			return;

		$current_user = wp_get_current_user();

		if ( get_user_meta( $current_user->ID, 'force-password-change', true ) ) {
			wp_redirect( admin_url( 'options-general.php?page=fs-reset-password' ) );
			exit; // never forget this after wp_redirect!
		}
	}

	/**
	 * If the user meta field is present, display an admin notice
	 * 
	 * @since 0.1
	 * @access private
	 */
	function notice() {

		global $current_user;

		wp_get_current_user();

		if ( get_user_meta( $current_user->ID, 'force-password-change', true ) ) {
			printf(
			'<div class="error"><p>%s</p></div>',
			__( 'Please change your password in order to continue using this website', 'force_password_change' )
			);
		}
	}
	
	/**
	 * Validate the password reset action.
	 * Set error messages to new user password reset form if needed.
	 * Delete the new user meta field and authorize regular admin access.
	 * 
	 * @since 0.1
	 * @access private
	 * 
	 */
	public function validate_password_reset() {
		if ( ! empty( $_POST ) && ! empty( $_POST['fs_reset_password_form'] ) ) {
			$error_messages = '';
			
			// Verify that the user is legitimate
			if ( ! is_user_logged_in() )
				wp_die( 'You are not allowed to view this resource.' );
			
			$current_user = wp_get_current_user();
			
			if ( empty( $current_user )	 )
				wp_die( 'Invalid user data provided.' );
			
			$username = $current_user->user_login;

			// Run some validations for the password and hashes
			if ( empty( $_POST['fs_res_password1'] ) || empty( $_POST['fs_res_password2'] ) ) {
				$error_messages .= "<p><span class='error'>ERROR:</span> Please fill in the password fields.</p>";
			} else if ( $_POST['fs_res_password1'] !== $_POST['fs_res_password2'] ) {
				$error_messages .= "<p><span class='error'>ERROR:</span> Passwords don't match.</p>";
			} else if ( 4 !== ForSite_Force_Passwords::slt_fsp_password_strength( $_POST['fs_res_password1'], $username ) ) {
				$error_messages .= "<p><span class='error'>ERROR:</span> Please make the password a strong one and at least 8 characters long.</p>";
			}
			if ( ! wp_verify_nonce( $_POST['fs_new_password'], 'set-new-password' ) ) {
				$error_messages .= "<p><span class='error'>ERROR:</span> Invalid security token.</p>";
			}
				
			// Try to authorize the user
			if ( empty( $error_messages ) ) {
				$password = $_POST['fs_res_password1'];
			
				$updated_user = wp_update_user( array ( 'ID' => $current_user->ID, 'user_pass' => $password ) );
			
				if( is_wp_error( $updated_user ) ) {
					$error_messages .= "<p><span class='error'>ERROR:</span> Unable to update user password.</p>";
				} else {
					delete_user_meta( $current_user->ID, 'force-password-change' );
					// Redirect to dashboard
					wp_redirect( admin_url( 'index.php' ) );
					exit;
				}
			}

			// Set the error messages displayed in the password reset form
			define( 'FS_PASS_RESET_MESSAGES', $error_messages );
		} 
	}
}

new ForSite_Password_Change();
