<?php
/**
 * 
 * Helper class for implementing the "Last login" feature.
 * 
 * Store the last login for a user and display it in the user's dashboard.
 * 
 * @since 0.1
 *
 */
class ForSite_Dashboard_Widget {
	
	/**
	 * Initialize the dashboard and filter for logins
	 */
	public function __construct() {
		add_action( 'wp_login', array( $this, 'manage_successful_logins' ), 99, 2 );
		add_action( 'wp_dashboard_setup', array( $this, 'last_login_widget' ) );
	}
	
	/**
	 * Add a dashboard widget displaying the last login time.
	 * 
	 * @since 0.1
	 * @access public
	 * @uses add_meta_box()
	 * 
	 * 
	 */
	public function last_login_widget() {
	
		add_meta_box( 
			'last_login_widget',
			'Last Login',
			array( $this, 'last_login_widget_callback' ),
			'dashboard',
			'normal',
			'high' );
		
	}
	
	/**
	 * Display the login widget in the dashboard.
	 * 
	 * Fetch the metadata for the user and display the 
	 * last login date.
	 * 
	 * @since 0.1
	 * 
	 */
	public function last_login_widget_callback() {
		if ( ! is_user_logged_in() )
			wp_die( "User should be logged in." );
		
		$user_id = get_current_user_id();
		
		$successful_logins = get_user_meta( $user_id, 'fs_successful_logins', true );
		
		if ( empty( $successful_logins ) )
			$successful_logins = array();
		
		$last_login = array(
			'current_login' => current_time( 'timestamp' ),
			'user_ip' 		=> $_SERVER['REMOTE_ADDR'],
			'user_rdns' 	=> gethostbyaddr( $_SERVER['REMOTE_ADDR'] )
		);
		
		if ( isset( $successful_logins[1] ) ) {
			$last_login = $successful_logins[1];
		} else if ( isset( $successful_logins[1] ) ) {
			$last_login = $successful_logins[0];
		}
		$login_date = date( 'Y-m-d H:i:s', $last_login['current_login'] );
		$user_ip = $last_login['user_ip'];
		$user_rdns = $last_login['user_rdns'];
		$current_user = wp_get_current_user();
		$display_name = $current_user->display_name;

		echo "Hello $display_name, your last login was on: $login_date from $user_ip ($user_rdns).";
	}
	
	/**
	 * Manage successful logins in the usermeta table.
	 * 
	 * Store the last 2 entries - current and previous.
	 * 
	 * @since 0.1
	 * @param string $user_login username
	 * @param WP_User $user the user object
	 */
	public function manage_successful_logins( $user_login, $user ) {
		$successful_logins = get_user_meta( $user->ID, 'fs_successful_logins', true );
		
		if ( empty( $successful_logins ) )
			$successful_logins = array();
		else
			$successful_logins[1] = $successful_logins[0];
		
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$user_rdns = gethostbyaddr( $user_ip );
		$current_login = current_time( 'timestamp' );

		$successful_login = array(
			'current_login' => $current_login,
			'user_ip' 		=> $user_ip,
			'user_rdns' 	=> $user_rdns
		);
		
		$successful_logins[0] = $successful_login;
		
		update_user_meta( $user->ID , 'fs_successful_logins', $successful_logins );
	}
}

new ForSite_Dashboard_Widget();
