<?php
/*
 This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Make sure we don't expose any info if called directly
if ( ! function_exists( 'add_action' ) ) {
	_e( "Hi there! I'm just a plugin, not much I can do when called directly.", 'slt-force-strong-passwords' );
	exit;
}

/**
 * 
 * Helper class for stronger passwords.
 * 
 * On password set/reset validate that a password would be
 * secure enough and at least 8 characters long.
 * 
 *
 * @since 0.1
 */
class ForSite_Force_Passwords {
	
	/**
	 * 
	 * List capabilities for the user level
	 * 
	 * @since 0.1
	 * @access private
	 * @var array
	 */
	private $fsp_capabilities;

	/**
	 * Initialize the hooks and variables for the strength meter 
	 */
	public function __construct() {
		$this->fsp_capabilities = 'publish_posts,upload_files,edit_published_posts';
		
		// Hook onto profile update to check user profile update and throw an error if the password isn't strong
		add_action( 'user_profile_update_errors', array( $this, 'slt_fsp_validate_profile_update' ), 0, 3 );
		
		// Hook onto password reset screen
		add_action( 'validate_password_reset', array( $this, 'slt_fsp_validate_strong_password' ), 10, 2 );
		
	}
	
	/**
	 * Callback method to validate profile update password on user profile screen
	 * 
	 * @since 0.1
	 * @access public
	 * @param array $errors list of errors while updating a profile
	 * @param bool $update
	 * @param WP_User $user_data updated user object data
	 * 
	 * @return array array of validation errors while updating the password
	 * 
	 */
	public function slt_fsp_validate_profile_update( $errors, $update, $user_data ) {
		return $this->slt_fsp_validate_strong_password( $errors, $user_data );
	}
	
	/**
	 * Main method for enforcing a strong password
	 * 
	 * @since 0.1
	 * @access public
	 * @param array $errors list of errors while updating a profile
	 * @param WP_User $user_data updated user object data
	 * 
	 * @return array array of validation errors while updating the password
	 */
	public function slt_fsp_validate_strong_password( $errors, $user_data ) {
		$password = ( isset( $_POST[ 'pass1' ] ) && trim( $_POST[ 'pass1' ] ) ) ? $_POST[ 'pass1' ] : false;
		$role = isset( $_POST[ 'role' ] ) ? $_POST[ 'role' ] : false;
		$user_id = isset( $user_data->ID ) ? $user_data->ID : false;
		$username = isset( $_POST["user_login"] ) ? $_POST["user_login"] : $user_data->user_login;
	
		// No password set?
		if ( false === $password )
			return $errors;
	
		// Already got a password error?
		if ( $errors->get_error_data("pass") )
			return $errors;
	
		// Should a strong password be enforced for this user?
		$enforce = true;
		if ( $user_id ) {
			// User ID specified
			$enforce = $this->slt_fsp_enforce_for_user( $user_id );
		} else {
			// No ID yet, adding new user - omit check for "weaker" roles
			if ( $role && in_array( $role, apply_filters( 'slt_fsp_weak_roles', array( "subscriber", "contributor" ) ) ) )
				$enforce = false;
		}
	
		// If enforcing and the strength check fails, add error
		if ( $enforce && $this->slt_fsp_password_strength( $password, $username ) != 4 )
			$errors->add( 'pass', apply_filters( 
					'slt_fsp_error_message', 
					__( '<strong>ERROR</strong>: Please make the password a strong one and at least 8 characters long.', 'slt-force-strong-passwords' ) ) );
	
		return $errors;
	}
	
	
	/**
	 * Always enforce strong passwords for users
	 *
	 * @since	0.1
	 * @access private
	 * @uses	apply_filters()
	 * @uses	user_can()
	 * @param	int	$user_id A user ID
	 * @return	boolean	enforce passwords for the given user or not
	 */
	private function slt_fsp_enforce_for_user( $user_id ) {
		$enforce = true;
		
		return $enforce;
	}
	
	/**
	 * Check for password strength - based on JS function in WP core: /wp-admin/js/password-strength-meter.js
	 *
	 * @since	0.1
	 * @access	public
	 * @param	string $i 	The password
	 * @param	string $f 	The user's username
	 * @return	integer	1 = very weak; 2 = weak; 3 = medium; 4 = strong
	 */
	public static function slt_fsp_password_strength( $i, $f ) {
		$h = 1; $e = 2; $b = 3; $a = 4; $d = 0; $g = null; $c = null;
		if ( strlen( $i ) < 8 )
			return $h;
		if ( strtolower( $i ) == strtolower( $f ) )
			return $e;
		if ( preg_match( "/[0-9]/", $i ) )
			$d += 10;
		if ( preg_match( "/[a-z]/", $i ) )
			$d += 26;
		if ( preg_match( "/[A-Z]/", $i ) )
			$d += 26;
		if ( preg_match( "/[^a-zA-Z0-9]/", $i ) )
			$d += 31;
		$g = log( pow( $d, strlen( $i ) ) );
		$c = $g / log( 2 );
		if ( $c < 40 )
			return $e;
		if ( $c < 56 )
			return $b;
		return $a;
	}
}

new ForSite_Force_Passwords();
