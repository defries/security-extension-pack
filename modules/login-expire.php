<?php
/**
 * Helper class to force login timeout
 *
 * Add a timeout of 15 minutes for all idle users.
 * 
 * @since 0.1
 *
 */
class ForSite_Login_Expire {
	
	/**
	 * 
	 * @var int min_update_timeout 5min before reupdating the DB value
	 */
	private $min_update_timeout = 300;
	
	/**
	 * 
	 * @var int max_update_timeout 15min for longest idle time
	 */
	private $max_timeout = 900;
	
	/**
	 * Initialize the login expiration
	 */
	public function __construct() {
		add_action( 'wp_login', array( $this, 'add_login_expiration' ), 10, 2 );
		add_action( 'init', array( $this, 'reset_login_counter' ) );
		
		add_filter( 'heartbeat_received', array( $this, 'validate_user_activity' ), 10, 2 );
		add_action( 'wp_enqueue_scripts' , array( $this, 'add_heartbeat_listener' ) );
// 		add_filter( 'auth_cookie_expiration', array( $this, 'rd_expire_login' ), 10 );
	}
	
	/**
	 * Adding a heartbeat API listener for the frontend
	 * 
	 * @since 0.2
	 */
	public function add_heartbeat_listener() {
		if ( is_user_logged_in() ) {
			wp_enqueue_script( 'heartbeat' );
			wp_enqueue_script( 'fs-login-verifier', plugins_url( '/js/fs-login-verifier.js', __FILE__ ), array( 'jquery', 'heartbeat' ) );
		}
	}
	
	/**
	 * Manage the login counter process
	 * 
	 * Update the idle timeout after 5min or logout for more than 15min
	 * 
	 * @since 0.2
	 */
	public function reset_login_counter() {
		// For logged in users
		if ( is_user_logged_in() && ! defined( 'DOING_AJAX' ) ) {
			$user_id = get_current_user_id();
		
			$time_diff = $this->get_time_diff( $user_id );

			// Update timeout where 5 < last login < 15
			if ( $time_diff > $this->min_update_timeout && $time_diff < $this->max_timeout ) {
				update_user_meta( $user_id, 'fs_last_active_time', time() );
			} else if( $time_diff > $this->max_timeout ) {
				// Logout users who was last active more than 15min ago
				wp_logout();
				if ( is_admin() ){
					wp_redirect( wp_login_url() );
					exit;
				}
			}
			
		}
	}
	
	/**
	 * Set login expiration to 15 minutes
	 *
	 * @since 0.1
	 * @access public
	 * @return string Expiration time in seconds
	 */
	public function rd_expire_login() {
		$expiration = $this->max_timeout;
		return $expiration;
	}
	
	/**
	 * The login expiration user meta key
	 * 
	 * @param string $user_login
	 * @param WP_User $user
	 * 
	 * @since 0.2
	 */
	public function add_login_expiration( $user_login, $user ) {
		update_user_meta( $user->ID , 'fs_last_active_time', time() );
	}
	
	/**
	 * Validate the user activity - heartbeat model
	 * 
	 * fsdata is ForSite data, set by the frontend call to trigger the callback
	 * 
	 * @param array $response response to be received by frontend
	 * @param array $data incoming data
	 * @return array $response object
	 * 
	 * @since 0.2
	 */
	public function validate_user_activity( $response, $data ) {
		// Ensure that it's our call
		if ( isset( $data['fsdata'] ) ) {
			$response['fsdata'] = 'logged_in';
			$time_diff = $this->get_time_diff( get_current_user_id() );
			
			if( $time_diff > $this->max_timeout ) {
				// Logout users who was last active more than 15min ago
				wp_logout();
				$response['fsdata'] = 'logged_out';
				$response['fsloginform'] = wp_login_url();
			}
		}
		
		return $response;
	}
	
	/**
	 * Helper function to get the diff between the time in the user meta
	 * and the current time 
	 * 
	 * @param int $user_id WP_User ID
	 * @return int timeout
	 */
	private function get_time_diff( $user_id ) {
		$last_active_time = get_user_meta( $user_id, 'fs_last_active_time', true );
			
		// Continue if login time not set
		if ( empty( $last_active_time ) ) {
			return 0;
		}
		
		// Get time diff between now and
		$time_diff = time() - $last_active_time;
		
		return $time_diff;
	}
}

new ForSite_Login_Expire();
