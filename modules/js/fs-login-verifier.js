jQuery(document).ready(function($) {
	// Debugging speed up only
	// wp.heartbeat.interval( 'fast' );
	
	// Mandatory. Otherwise hooks are not fired.
	jQuery(document).on('heartbeat-send', function(e, data) {
		data['fsdata'] = 'trigger';
	});
	
	$(document).on( 'heartbeat-tick.fsdata', function( e, data ) {
		// redirect to form after expiration
		if( data.hasOwnProperty('fsdata') && data['fsdata'] === 'logged_out' ) {
			window.location.replace( data['fsloginform'] );
		}
	});
});