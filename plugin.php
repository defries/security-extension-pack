<?php
/*
Plugin Name: Security Extension Pack
Plugin URI: https://github.com/forsitemedia/security-extension-pack
Description: Adds extra security features for your site
Author: Forsite Themes
Version: 0.1-soalphathatithurts
Author URI: http://forsitethemes.com/

License: GPLv2 ->

  Copyright 2012-2014 Forsite Themes (team@forsitethemes.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


/*
 * Load the Extension pack framework
 */
if ( ! class_exists( 'Forsite_Extension_Pack' ) ) {
	require( 'extension-pack/extension-framework.php' );
}

/*
 * 
 */
class Forsite_Security_Extension extends Forsite_Extension_Pack {

	public $option_name = 'fse_modules';
	public $page_name = 'display_fse_admin_page';
	public $admin_url;
	public $modules_dir;
	public $modules_url;
	public $nonce = 'fse-nonce';
	public $page_title;
	public $menu_title;
	public $menu_icon;
	public $menu_position = "3.05"; // This needs to be unique
	public $modules_page_title;
	public $modules_menu_title;
	public $submodules_page_name = 'display_fse_modules_page';
	public $module_defaults;

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {

		// Setting definitions (used by some individual modules)
		define( 'FSE_PACK_DIR', dirname( __FILE__ ) );
		define( 'FSE_PACK_URL', plugin_dir_url( __FILE__ ) );

		// Set variables used by the extension pack framework
		$this->modules_dir        = dirname( __FILE__ );
		$this->modules_url        = plugin_dir_url( __FILE__ );
		$this->page_title         = __( 'Security', 'fse' );
		$this->menu_title         = __( 'Security', 'fse' );
		$this->menu_icon          = $this->modules_url . 'assets/icon.png';
		$this->modules_page_title = __( 'Modules', 'fse' );
		$this->modules_menu_title = __( 'Modules', 'fse' );
		$this->module_defaults();

		// Set admin URL (two options to choose from depending on the current page)
		if ( isset( $_GET['page'] ) && ( $this->submodules_page_name == $_GET['page'] || $this->page_name == $_GET['page'] ) ) {
			$slug = $_GET['page'];
			$this->admin_url = admin_url() . 'admin.php?page=' . $slug;
		}

		// Load text string
		load_plugin_textdomain( 'fse', false, $this->modules_url . '/assets/languages' );

		// Load the Extension Pack's contructor
		parent::__construct();

	}

	/**
	 * Sets the details of modules that are available.
	 * The values below are considered the defaults, and are overridden by what is stored
	 * in the fse_modules setting
	 */
	public function module_defaults() {

		$this->module_defaults = array(
			0 => array(
				'show_image' => true,
				'id' => 'expire-passwords',
				'name' => __( 'Expire passwords', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/expire-passwords/expired.png',
				),
				'description' => __( 'Sets authorization for the passwords for 90 days.', 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			1 => array(
				'show_image' => true,
				'id' => 'force-password-change',
				'name' => __( 'Force password change', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/force-password-change/thumbnail.jpg',
				),
				'description' => __( 'Change password on first login to site after a new user is created', 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular, widget',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			2 => array(
				'show_image' => true,
				'id' => 'force-strong-passwords',
				'name' => 'force-strong-passwords',
				'screenshot' => array(
					0 => $this->modules_url . 'modules/force-strong-passwords/thumbnail.png',
				),
				'description' => __( 'On password set/reset validate that a password would be * secure enough and at least 8 characters long', 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			3 => array(
				'show_image' => true,
				'id' => 'header-controller',
				'name' => __( 'Header controller', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/header-controller/thumbnail.png',
				),
				'description' => __( 'For managing headers for security reasons.', 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			4 => array(
				'show_image' => true,
				'id' => 'login-dashboard-widget',
				'name' => 'Login dashboard-widget',
				'screenshot' => array(
					0 => $this->modules_url . 'modules/login-dashboard-widget/thumbnail.jpg',
				),
				'description' => __( "Store the last login for a user and display it in the user's dashboard.", 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular, widget',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			5 => array(
				'show_image' => true,
				'id' => 'login-expire',
				'name' => __( 'Login expire', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/login-expire/thumbnail.gif',
				),
				'description' => __( 'Adds a timeout of 15 minutes for all idle users.', 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular,',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			6 => array(
				'show_image' => true,
				'id' => 'login-message',
				'name' => __( 'Login message', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/login-message/thumbnail.png',
				),
				'description' => __( 'For displaying a login message * above the login form.' , 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			7 => array(
				'show_image' => true,
				'id' => 'prevent-old-passwords',
				'name' => __( 'Prevent old passwords', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/prevent-old-passwords/thumbnail.png',
				),
				'description' => __( 'Prevent using one of the previous 12 passwords that the user has set.' , 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
			8 => array(
				'show_image' => true,
				'id' => 'user-disable',
				'name' => __( 'User disable', 'fse' ),
				'screenshot' => array(
					0 => $this->modules_url . 'modules/user-disable/thumbnail.png',
				),
				'description' => __( 'Prevents against brute force attacks, allows for manually locking/disabling users.' , 'fse' ),
				'author' => 'Test author',
				'authorAndUri' => '<a href="http://ryanhellyer.net/" title="Test author">Test author</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'configure' => 'options-discussion.php',
				'introduced' => '1.0',
			),
		);
	}

}

new Forsite_Security_Extension(); // Fire her up, baby!
